#include <LiquidCrystal.h>

#define btAcceptar  A0
#define btCancelar  A2
#define btInit      A1

#define LED1        10
#define LED2         9
#define LED3         8
#define LED4         7
#define LED5         6
#define pinMotor    A3
#define pinSo       A4

//lcd
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
  
  lcd.begin(20, 4);
  Serial.begin(115200);

  pinMode(LED1, OUTPUT); 
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(LED4, OUTPUT);
  pinMode(LED5, OUTPUT);
  pinMode(pinSo,OUTPUT);
  pinMode(pinMotor,OUTPUT);

  pinMode(btAcceptar, INPUT);
  pinMode(btCancelar, INPUT);
  pinMode(btInit, INPUT);  
}

void loop() {

  if(digitalRead(btInit)){
    lcd.clear();
    lcd.print("Quieres insertar    ");
    lcd.print(" SI              NO ");
    lcd.print("pastillas nuevas?   ");
    lcd.print("                    ");
    
    long long int msegs = millis();
    int ok = 0;
    int cancel = 0;
    
    while(msegs+10000 >= millis() && ok == 0 && cancel == 0) {
      ok = digitalRead(btAcceptar);
      cancel = digitalRead(btCancelar);
    }
    
    lcd.clear();
    
    if(ok == 1) {
      Serial.print("readNFC\n");
      delay(1200);
      while (!Serial.available());
      while (Serial.peek() != '?') Serial.read();
      Serial.read();
    
      int n = Serial.read()-'0';
    
      for (int i = 0; i < n; ++i) {

        String str = Serial.readStringUntil('#');
        String nom = str.substring(0, str.indexOf('&'));
        String quantitat = str.substring(str.indexOf('&')+1, str.length());

        while (nom.length() < 20) nom.concat(" ");
        
        lcd.clear();
        lcd.print("Insertar ");
        lcd.print(quantitat);
        lcd.print(" past. de");
        lcd.print("en comparitmiento ");
        lcd.print(i+1);
        lcd.print(" ");
        lcd.print(nom);
        lcd.print("  Siguiente         ");  

        

        if (i == 0) {
          digitalWrite(LED1, HIGH);
        }
        else if (i == 1) {
          digitalWrite(LED1, LOW);
          digitalWrite(LED2, HIGH);
        }
        else if (i == 2) {
          digitalWrite(LED2, LOW);
          digitalWrite(LED3, HIGH);
        }
        else if (i == 3) {
          digitalWrite(LED3, LOW);
          digitalWrite(LED4, HIGH);
        }
        else if (i == 4) {
          digitalWrite(LED4, LOW);
          digitalWrite(LED5, HIGH);
        }
        
        delay(1000);
        while(!digitalRead(btAcceptar)) delay(200);
        delay(1000);

      }

      lcd.clear();
      lcd.print("Procedimiento       ");
      lcd.print("                    ");
      lcd.print("terminado           ");
      lcd.print("                    ");

      digitalWrite(LED1, LOW);
      digitalWrite(LED2, LOW);
      digitalWrite(LED3, LOW);
      digitalWrite(LED4, LOW);
      digitalWrite(LED5, LOW);
      delay(1000);
    }
    else if(cancel == 1) lcd.print("Cancelar");
    else lcd.print("Cancelado por tiempo");
  }


  if(Serial.available() && Serial.read() == 'A'){
    while(!Serial.available());
    int pin = 10 - (Serial.read() - '1');
    digitalWrite(pin, HIGH);
    
    bool rep = medicament();
    while(rep == 0){
      delay(200);
      rep = medicament();
    }
    digitalWrite(pin, LOW);
  }

  
  delay(800);
  displayDefault();
}


bool medicament() {
  bool final;
  lcd.clear();
  lcd.print("Quiere la pastilla? ");
  lcd.print(" SI              NO ");
    
  int ok = 0;
  int cancel = 0;

  
  while(ok == 0 && cancel == 0){
    ok = digitalRead(btAcceptar);
    cancel = digitalRead(btCancelar);
    digitalWrite(pinSo, HIGH);
    delay(200);    
    digitalWrite(pinSo, LOW);
  }
  delay(1000);
     
  if(cancel == 1){
    lcd.clear();
    lcd.print("Seguro?             ");
    lcd.print(" SI              NO ");
    delay(200);

    do {
      ok = digitalRead(btAcceptar);
      cancel = digitalRead(btCancelar);
      delay(200);
    }
    while(ok == 0 && cancel == 0);
     
    if(ok == 1) return 1;
    else return 0;
  }

  lcd.clear();
  lcd.print("                    ");
  lcd.print("    SALIENDO...     ");
  
  digitalWrite(pinMotor, HIGH);
  delay(2000);
  digitalWrite(pinMotor, LOW);
  
  return 1;
}


void displayDefault() {
  int aa = 0;
  bool t = false;
  
  while (!Serial.available() && aa < 10000) ++aa;
  while (Serial.peek() == '@'){
    Serial.read();
    t = true;
  }

  if (t) {
    char buffer[9];
    Serial.readBytesUntil('#', buffer, 8);
    buffer[8] = '\0';
    lcd.clear();
    lcd.print("   RELOJ   ");
    lcd.print(buffer);
    lcd.print(" ");
    
    lcd.print("  SIGUIENTE MED.:   ");
    lcd.print("                    ");
    lcd.print(" DOMINGO    18:30   ");
  }
}

