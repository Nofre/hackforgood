import time
import serial
from datetime import datetime
#from SimpleHTTPServer import SimpleHTTPRequestHandler
#from BaseHTTPServer import HTTPServer
#import threading

class Medicament:
    def __init__(self, name, d1, d2, d3, d4, d5, d6, d7, length, n, total):
        self.name = name
        self.dies = []
        self.dies.append(d1)
        self.dies.append(d2)
        self.dies.append(d3)
        self.dies.append(d4)
        self.dies.append(d5)
        self.dies.append(d6)
        self.dies.append(d7)
        self.length = length
        self.n = n
        self.total = total

medicaments = []

medicaments.append(Medicament("Ibuprofeno", "", "" , "8:00,16:00", "", "", "", "21:00", 14, 1, 20))
medicaments.append(Medicament("Paracetamol", "", "" , "8:00,16:00", "", "", "", "21:00", 14, 1, 30))
medicaments.append(Medicament("Droga", "", "" , "8:00,16:00", "", "16:28", "", "21:00", 14, 1, 15))


#server = HTTPServer(('', 8080), SimpleHTTPRequestHandler)
#thread = threading.Thread(target = server.serve_forever)
#thread.daemon = True
#try:
#    thread.start()
#except KeyboardInterrupt:
#    server.shutdown()
#    sys.exit(0)


# configure the serial connections (the parameters differs on the device you are connecting to)
serial = serial.Serial(
    port='COM3',
    baudrate=115200,
    timeout=0
)
serial.close()
serial.open()

while 1:

    cmd = serial.readline()

    if len(cmd) > 0:
        print "R >> "  + cmd

    if cmd == "readNFC\n":
        serial.write("?"+str(len(medicaments)))
        for medicament in medicaments:
            serial.write(str(medicament.name)+"&"+str(medicament.total)+'#')


    now = datetime.now()
    
    for medicament in medicaments:

        i = 0
        
        dia = medicament.dies[datetime.today().weekday()]
        
        hores = dia.split(",")

        for hora in hores:
            if hora != "":
                h = int(hora.split(":")[0])
                m = int(hora.split(":")[1])

                if h == now.hour and m == now.minute and now.second == 40:
                    dia_hora = next_day()
                    print "S >> A"
                    serial.write("A"+str(i)+dia_hora)

        ++i

    formated_time = "@"
    if now.hour < 10:
        formated_time+="0"
    formated_time+=str(now.hour)
    formated_time+=":"
    if now.minute < 10:
        formated_time+="0"
    formated_time+=str(now.minute)
    formated_time+=":"
    if now.second < 10:
        formated_time+="0"
    formated_time+=str(now.second)
    formated_time+="#"

    serial.write(formated_time)

    time.sleep(0.9)
    
    def next_day():
        day_hour = "999999"
        dies_probables = []
        i = datetime.today().weekday()
        for medicament in medicaments:
            x = 0
            for dia in medicament.dies: 
                if x >= i:
                    hores = dia.split(",")
                    for hora in hores:
                        if hora != "":
                            h = int(hora.split(":")[0])
                            m = int(hora.split(":")[1])   
                    
                            if h > now.hour and m > now.minute and now.second > 40:
                                a.append(i + "#" + hora)
                ++x
        for final in dies_probables:
            if final < day_hour:
                day_hour = final
        
    return day_hour